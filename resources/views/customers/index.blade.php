@extends('layouts.app')
@section('content')

        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<head>
    
</head>

        <div class='container'>
            <h2>Customers list </h2>
                <br/>
                
               <p style="font-family: Arial, Helvetica, sans-serif,font-size: 20px"> Hello {{auth()->user()->name}} ,</h4>
              <p style="font-family: Arial, Helvetica, sans-serif ,font-size: 20px">role type : {{auth()->user()->role}}</p>
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Mail</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Create user</th>
                        <th scope="col">Edit</th> 
                        <th scope="col">Delete</th> 
                        <th scope="col">Status</th> 
                        </tr>
                    </thead>

                   
                    <tbody>
                    @foreach($customers as $customer)
                            <tr>

                            @if ($customer->user_id == $id)
                                @php $bold = 'font-weight:bold' @endphp
                            @else 
                                @php $bold = '' @endphp
                            @endif 

                                <td style = "{{ $bold }}" >  {{$customer->name}} </td>

                                <td style = "{{ $bold }}">  {{$customer->email}}  </td>

                                <td style = "{{ $bold }}">  {{$customer->phone}}  </td> 

                                <td style = "{{ $bold }}">  {{$customer->username}}  </td> 

                                <td style = "{{ $bold }}">  <a href="{{route('customers.edit', $customer->id)}}">Edit</a>    </td>
                             
                                @can('manager')
                                   <td style = "{{ $bold }}" ><a href="{{route('delete', $customer->id)}}">Delete</a></td>
                                @endcan
                            
                                @cannot('manager')
                                    <td style = "{{ $bold }}">Delete</a></td>
                                @endcannot
                             
                               
                                @can('manager')
                                    @if ($customer->status == 0)
                                           <td style = "{{ $bold }}" ><a href="{{route('change', $customer->id)}}"> close deal</a></td>
                                    @elseif($customer->status == 1)
                                     <td style = "{{ $bold }}", bgcolor="#00FF00"></td>
                                @endcan 
                                @elseif($customer->status == 0)
                                 <td style = "{{ $bold }}">   <p>you can't allowed to close deals<p></td>
                                    @elseif($customer->status == 1)
                                    <td bgcolor="#00FF00" ,style = "{{ $bold }}" >   <p><p></td>
                                    @endif
                                
                                
         
                            </tr>
                    @endforeach

                    </tbody>
                    </table>

                    <div class ="container">
                          <div class="col-5  offset-5">
                             <a href="{{route('customers.create')}}" class=" btn btn-secondary">Add another customer to your list</a>
                         </div>
                            
                            
                    </div>
                
            </div>
@endsection
