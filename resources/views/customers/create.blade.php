@extends('layouts.app')
@section('content')

<h1> Create a new customer </h1>
<form method = 'post' action = "{{action('CustomersController@store')}}"  >
{{csrf_field()}}
<div class="col-4 offset-3">

<div class = "form-group">

    <label for = "name :"> name :</label>
    <input type = "text" class = "form-control" name = "name">

    <label for = "email"> Email : </label>
    <input type = "text" class = "form-control" name = "email">

    <label for = "price"> Phone </label>
    <input type = "number" class = "form-control" name = "phone">
</div>
</div>
            @if ($errors->any())
             <div class="alert alert-danger">
                        <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                        </ul>
                        </div>
                @endif

<br><br>
 <div class="col-2 offset-4">
        <div class = "form-group">
        <input type = "submit" class= "form-control" name="submit" value= "save">
        </div>
</div>

</form>

@endsection