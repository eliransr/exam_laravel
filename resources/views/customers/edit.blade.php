@extends('layouts.app')
@section('content')
    
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        </head>
     <!--Update-->

        <div class="container">

                <br/><br/>
                <h3>Edit customer</h3>
            
                
                <form method="post" action ="{{action('CustomersController@update' , $customer->id)}}">
                {{csrf_field()}}
                    @method('PATCH')
                    <div class="col-4 offset-3">
                        <div class="form-group">
                          <label for="male">Name :</label>
                            <input type ="text" class ="form-control" name="name" value = "{{$customer->name}}">
                        </div>
                        <div class="form-group">
                        <label for="male">Mail :</label>
                            <input type ="text" class ="form-control" name="email" value = "{{$customer->email}}">
                        </div>
                        <div class="form-group">
                        <label for="male">Phone :</label>
                            <input type ="text" class ="form-control" name="phone" value = "{{$customer->phone}}">
                        </div>
                    </div>  
                        

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        
                        <br>
                        <div class ="container">
                            <div class="col-4  offset-4">
                                <input type ="submit" class="form-control btn btn-secondary" name="submit" value =" save "> 
                            </div>
                        </div>
                </form>
        </div>
        
        
    

        <br><br>
        <div class="container">
        <form>
        
                <div class ="container">
                    <div class="col-4  offset-4">
                        <a href="{{route('customers.index')}}" class=" form-control btn btn-secondary">Back to list</a>
                    </div>
                </div>
        </form>
        </div>
        @endsection
 