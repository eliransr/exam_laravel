<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
     //database fields to be allwed for massive assigment
     protected $fillable =[
        'name',
        'email',
        'phone',
        'status',
        
    ];
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}
