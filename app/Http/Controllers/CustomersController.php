<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\User;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $customers = Customer::all();
        return view('customers.index' ,  compact('customers' , 'id'));
       
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required']);

            $customer = new Customer();
          $id=Auth::id();
          $boss = DB::table('users')->where('id',$id)->first();
           $username = $boss->name;
           $customer->name = $request->name;
           $customer->email = $request->email;
           $customer->phone = $request->phone;
           $customer->status=0;
           $customer->user_id = $id;
           $customer->username = $username;
           $customer->save();
           return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        $id = Auth::id();
        
            if (Gate::denies('manager')and ($customer->user_id != $id)) {
                abort(403,"Sorry, you do not hold permission to edit this customer");
            } 
        
        return view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id); 

        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required'
            ]);

            
                       
            $customer->update($request->except(['_token'])); 
            return redirect('customers');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);

        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to delete customers"  );
        }

        if(!$customer->user->id ==Auth::id()) 
        {
            return (redirect('books'));
        }
        
        $customer->delete();
        return redirect('customers'); 
    }


    public function change($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to change status");
        } 
        $customer = Customer::findOrFail($id);            
        $customer->status = 1; 
        $customer->save();
        return redirect('customers');    
    }    


  
}
